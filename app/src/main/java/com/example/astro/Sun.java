package com.example.astro;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astrocalculator.AstroCalculator;

public class Sun extends Fragment {
    private static final String TAG = "DEBUG";

    TextView sunrise;
    TextView sunset;
    TextView morningTwilight;
    TextView eveningTwilight;
    TextView sunriseAzimuth;
    TextView sunsetAzimuth;

    String sunriseVal;
    String sunsetVal;
    String morningTwilightVal;
    String eveningTwilightVal;
    String sunriseAzimuthVal;
    String sunsetAzimuthVal;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("sunrise", sunriseVal );
        outState.putString("sunset", sunsetVal);
        outState.putString("morningTwilight", morningTwilightVal);
        outState.putString("eveningTwilight", eveningTwilightVal);
        outState.putString("sunriseAzimuth", sunriseAzimuthVal);
        outState.putString("sunsetAzimuth", sunsetAzimuthVal);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        sunriseVal = getArguments().getString("sunrise");
        sunsetVal = getArguments().getString("sunset");
        morningTwilightVal = getArguments().getString("morningTwilight");
        eveningTwilightVal = getArguments().getString("eveningTwilight");
        sunriseAzimuthVal = getArguments().getString("sunriseAzimuth");
        sunsetAzimuthVal = getArguments().getString("sunsetAzimuth");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sun_fragment, container, false);

        if(savedInstanceState != null){
            sunriseVal = savedInstanceState.getString("sunrise");
            sunsetVal = savedInstanceState.getString("sunset");
            morningTwilightVal = savedInstanceState.getString("morningTwilight");
            eveningTwilightVal = savedInstanceState.getString("eveningTwilight");
            sunriseAzimuthVal = savedInstanceState.getString("sunriseAzimuth");
            sunsetAzimuthVal = savedInstanceState.getString("sunsetAzimuth");
        }

        sunrise = (TextView) v.findViewById(R.id.sunrise);
        sunriseAzimuth = (TextView) v.findViewById(R.id.sunrise_azimuth);
        sunset = (TextView) v.findViewById(R.id.sunset);
        sunsetAzimuth = (TextView) v.findViewById(R.id.sunset_azimuth);
        morningTwilight = (TextView) v.findViewById(R.id.morning_twilight);
        eveningTwilight = (TextView) v.findViewById(R.id.evening_twilight);

        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        int orientation = getResources().getConfiguration().orientation;

        if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            sunrise.setTextSize(27);
            sunriseAzimuth.setTextSize(27);
            sunset.setTextSize(27);
            sunsetAzimuth.setTextSize(27);
            morningTwilight.setTextSize(27);
            eveningTwilight.setTextSize(27);
        }
        if (screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL && orientation == Configuration.ORIENTATION_PORTRAIT){
            sunrise.setTextSize(19);
            sunriseAzimuth.setTextSize(19);
            sunset.setTextSize(19);
            sunsetAzimuth.setTextSize(19);
            morningTwilight.setTextSize(19);
            eveningTwilight.setTextSize(19);
        }

        updateFragment();
        return v;
    }


    public void updateAstro(AstroCalculator.SunInfo sunInfo) {
//        Log.d(TAG, "called to sun fragemnt update");
        sunriseVal = timeFormatter(sunInfo.getSunrise().getHour(), sunInfo.getSunrise().getMinute());
        sunsetVal = timeFormatter(sunInfo.getSunset().getHour(), sunInfo.getSunset().getMinute());
        morningTwilightVal = timeFormatter(sunInfo.getTwilightMorning().getHour(), sunInfo.getTwilightMorning().getMinute());
        eveningTwilightVal = timeFormatter(sunInfo.getTwilightEvening().getHour(), sunInfo.getTwilightEvening().getMinute());

        sunriseAzimuthVal = azimuthFormatter(sunInfo.getAzimuthRise());
        sunsetAzimuthVal = azimuthFormatter(sunInfo.getAzimuthSet());
        updateFragment();
    }

    public void updateFragment() {
        Activity activity = getActivity();
        if(activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sunrise.setText("Wschód: " + sunriseVal);
                    sunset.setText("Zachód: " + sunsetVal);
                    morningTwilight.setText("Godzina świtu: " + morningTwilightVal);
                    eveningTwilight.setText("Godzina zmierzchu: " + eveningTwilightVal);
                    sunriseAzimuth.setText("Azymut wschodu: " + sunriseAzimuthVal);
                    sunsetAzimuth.setText("Azymut zachodu: " + sunsetAzimuthVal);
                }
            });
        }
    }

    public String timeFormatter(int hour, int minute) {
        return hour + ":" + (minute < 10 ? "0" + minute : minute);
    };

    public String azimuthFormatter(double azimuth) {
        return String.format("%.2f°", azimuth);
    }
}