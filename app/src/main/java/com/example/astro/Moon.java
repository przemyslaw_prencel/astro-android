package com.example.astro;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astrocalculator.AstroCalculator;

public class Moon extends Fragment {
    private static final String TAG = "DEBUG";

    TextView moonrise;
    TextView moonset;
    TextView nextMoon;
    TextView fullMoon;
    TextView illumination;
    TextView moonAge;

    String moonriseVal;
    String moonsetVal;
    String nextMoonVal;
    String fullMoonVal;
    String illuminationVal;
    String moonAgeVal;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        moonriseVal = getArguments().getString("moonrise");
        moonsetVal = getArguments().getString("moonset");
        nextMoonVal = getArguments().getString("nextMoon");
        fullMoonVal = getArguments().getString("fullMoon");
        illuminationVal = getArguments().getString("illumination");
        moonAgeVal = getArguments().getString("moonAge");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.moon_fragment, container, false);

        moonrise = (TextView) v.findViewById(R.id.moonrise);
        moonset = (TextView) v.findViewById(R.id.moonset);
        nextMoon = (TextView) v.findViewById(R.id.next_moon);
        fullMoon = (TextView) v.findViewById(R.id.full_moon);
        illumination = (TextView) v.findViewById(R.id.illumination);
        moonAge = (TextView) v.findViewById(R.id.moon_age);

        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        int orientation = getResources().getConfiguration().orientation;

        if(screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            moonrise.setTextSize(27);
            moonset.setTextSize(27);
            nextMoon.setTextSize(27);
            fullMoon.setTextSize(27);
            illumination.setTextSize(27);
            moonAge.setTextSize(27);
        }
        if (screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL && orientation == Configuration.ORIENTATION_PORTRAIT){
            moonrise.setTextSize(19);
            moonset.setTextSize(19);
            nextMoon.setTextSize(19);
            fullMoon.setTextSize(19);
            illumination.setTextSize(19);
            moonAge.setTextSize(19);
        }
//        if (screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL && orientation == Configuration.ORIENTATION_LANDSCAPE){
//            moonrise.setTextSize(14);
//            moonset.setTextSize(14);
//            nextMoon.setTextSize(14);
//            fullMoon.setTextSize(14);
//            illumination.setTextSize(14);
//            moonAge.setTextSize(14);
//        }

        updateFragment();
        return v;
    }


    public void updateAstro(AstroCalculator.MoonInfo moonInfo) {
//        Log.d(TAG, "called to moon fragemnt update");
        moonriseVal = timeFormatter(moonInfo.getMoonrise().getHour(), moonInfo.getMoonrise().getMinute());
        moonsetVal = timeFormatter(moonInfo.getMoonset().getHour(), moonInfo.getMoonset().getMinute());
        nextMoonVal = dateFormatter(moonInfo.getNextNewMoon().getDay(), moonInfo.getNextNewMoon().getMonth(), moonInfo.getNextNewMoon().getYear());
        fullMoonVal = dateFormatter(moonInfo.getNextFullMoon().getDay(), moonInfo.getNextFullMoon().getMonth(), moonInfo.getNextFullMoon().getYear());

        illuminationVal = String.format("%.2f ", moonInfo.getIllumination() * 100 ) + "%";
        moonAgeVal = (int) Math.round(moonInfo.getAge()*(-1)) + " dzień";

        updateFragment();
    }


    public void updateFragment() {
        Activity activity = getActivity();
        if (activity != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    moonrise.setText("Wschód: " + moonriseVal);
                    moonset.setText("Zachód: " + moonsetVal);
                    nextMoon.setText("Najblższy nów: " + nextMoonVal);
                    fullMoon.setText("Najbiższa pełnia: " + fullMoonVal);
                    illumination.setText("Faza ksiezyca: " + illuminationVal);
                    moonAge.setText("Dzień synodyczny: " + moonAgeVal);
                }
            });
        }
    }

    public String timeFormatter(int hour, int minute) {
        return hour + ":" + (minute < 10 ? "0" + minute : minute);
    };

    private String dateFormatter(int day, int month, int year) {
        return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + year;
    }
}