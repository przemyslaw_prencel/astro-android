package com.example.astro;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static java.lang.String.valueOf;

/**
 * Demonstration of using fragments to implement different activity layouts.
 * This sample provides a different layout (and activity flow) when run in
 * landscape.
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "DEBUG";
    private TypedArray refreshArraysValues;

    private AstroDateTime astroTime;
    private AstroCalculator.Location location;
    private AstroCalculator astro;

    private Timer fragmentTimer;


    private TextClock digitalClock;
    private TextView locElement;
    private Spinner latitudeSpinner;
    private EditText latitudeInput;
    private Spinner longitudeSpinner;
    private EditText longitudeInput;
    private Spinner refreshSpinner;
    private Button btnSubmit;


    private double latitude = 51.45;
    private double longitude = 19.28;
    private Integer timerInterval = 600;
    private Integer selectedTimerInterval;

    private ViewPager mPager;
    private PagerAdapter pagerAdapter;

    private Sun sun;
    private Moon moon;
    int screenSize;
    int orientation;

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "STOP");
        if(fragmentTimer != null)
            fragmentTimer.cancel();
    }
    @Override
    public void onResume() {
        super.onResume();
        refreshArraysValues = getResources().obtainTypedArray(R.array.refresh_arrays_values);
        // Screen size integer, large - Configuration.SCREENLAYOUT_SIZE_LARGE, small - Configuration.S
        screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        orientation = getResources().getConfiguration().orientation;

        initAstro();
        sun = buildSun();
        moon = buildMoon();


        fillEditLayoutValues();
        addListenerOnRefreshSpinner();
        addListenerOnButton();
        initAstro();
        locationPresenter(false);

        Log.d(TAG, "[onCreate] current orientation: "+ getResources().getConfiguration().orientation);
        if (orientation == Configuration.ORIENTATION_PORTRAIT && screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            mPager = (ViewPager) findViewById(R.id.pager);
            pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
            mPager.setAdapter(pagerAdapter);
        } else {

            FragmentManager fragmentManager = getSupportFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.sun_fragment, sun);
            fragmentTransaction.commit();

            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.moon_fragment, moon);
            fragmentTransaction.commit();
        }
        runFragmentRefresher(timerInterval);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putDouble("latitude", latitude);
        outState.putDouble("longitude", longitude);
        outState.putInt("timerInterval", timerInterval);
        latitudeInput.clearFocus();
        longitudeInput.clearFocus();

        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null) {
            for (Fragment frag : fragmentList )
                getSupportFragmentManager().beginTransaction().remove(frag).commit();
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        refreshArraysValues = getResources().obtainTypedArray(R.array.refresh_arrays_values);
        // Screen size integer, large - Configuration.SCREENLAYOUT_SIZE_LARGE, small - Configuration.S
        screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        orientation = getResources().getConfiguration().orientation;

        initializeLayoutElements();
        if (savedInstanceState != null) {
            latitude = savedInstanceState.getDouble("latitude");
            longitude = savedInstanceState.getDouble("longitude");
            timerInterval = savedInstanceState.getInt("timerInterval");
        }

        initAstro();
        sun = buildSun();
        moon = buildMoon();


        fillEditLayoutValues();
        addListenerOnRefreshSpinner();
        addListenerOnButton();
        initAstro();
        locationPresenter(false);

        Log.d(TAG, "[onCreate] current orientation: "+ getResources().getConfiguration().orientation);
        if (orientation == Configuration.ORIENTATION_PORTRAIT && screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            mPager = (ViewPager) findViewById(R.id.pager);
            pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
            mPager.setAdapter(pagerAdapter);
        } else {

            FragmentManager fragmentManager = getSupportFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.sun_fragment, sun);
            fragmentTransaction.commit();

            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.moon_fragment, moon);
            fragmentTransaction.commit();
        }
        runFragmentRefresher(timerInterval);

    }

    private void initializeLayoutElements() {
        locElement = findViewById(R.id.geo_element);
        digitalClock = findViewById(R.id.digitalClock);
        digitalClock.setFormat24Hour("HH:mm:ss");

        latitudeSpinner = (Spinner) findViewById(R.id.latitude_spinner);
        latitudeInput = (EditText) findViewById(R.id.latitude_input);
        longitudeSpinner = (Spinner) findViewById(R.id.longitude_spinner);
        longitudeInput = (EditText) findViewById(R.id.longitude_input);
        refreshSpinner = (Spinner) findViewById(R.id.refresh_spinner);
        btnSubmit = (Button) findViewById(R.id.btn_submit);

        if (screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            ArrayAdapter refeshAdapter = ArrayAdapter.createFromResource(this, R.array.refresh_arrays, R.layout.spinner_item);
            ArrayAdapter latitudeAdapter = ArrayAdapter.createFromResource(this, R.array.latitude_arrays, R.layout.spinner_item);
            ArrayAdapter logitudeAdapter = ArrayAdapter.createFromResource(this, R.array.longitude_arrays, R.layout.spinner_item);

            refeshAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            latitudeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            logitudeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            refreshSpinner.setAdapter(refeshAdapter);
            latitudeSpinner.setAdapter(latitudeAdapter);
            longitudeSpinner.setAdapter(logitudeAdapter);
        }
    }

    private void fillEditLayoutValues() {
        latitudeInput.setText(valueOf(latitude));
        longitudeInput.setText(valueOf(longitude));

        latitudeSpinner.setSelection((latitude > 0) ? 0 : 1);
        longitudeSpinner.setSelection((longitude > 0) ? 0 : 1);

        // boze jakie to pryitywne
        for(int i = 0; i < refreshArraysValues.length(); i++){
            Integer value = refreshArraysValues.getInt(i, -1);
            if(value.equals(timerInterval))
                refreshSpinner.setSelection(i);
        }
    }


    private void addListenerOnRefreshSpinner() {

        refreshSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedTimerInterval = refreshArraysValues.getInt(position, -1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void sendToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public void addListenerOnButton() {

        btnSubmit.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View v) {
          btnSubmit.onEditorAction(EditorInfo.IME_ACTION_DONE);
          latitudeInput.clearFocus();
          longitudeInput.clearFocus();
          btnSubmit.requestFocus(); //or any other View
          double newLatitudeValue;
          double newLongitudeValue;
          try {
              newLatitudeValue = Double.parseDouble(valueOf(latitudeInput.getText()));
              newLongitudeValue = Double.parseDouble(valueOf(longitudeInput.getText()));
              newLatitudeValue = Math.round(newLatitudeValue * 10000.0) / 10000.0;
              newLongitudeValue = Math.round(newLongitudeValue * 10000.0) / 10000.0;
          }catch (NumberFormatException e){
              sendToast("Niepoprawy format wprowadonych danych");
              return;
          }
          boolean validLatitude = (latitudeInput.getText().length() > 0) && (newLatitudeValue >= 0) && (newLatitudeValue <= 180);
          boolean validLongitude = (longitudeInput.getText().length() > 0) && (newLongitudeValue >= 0) && (newLongitudeValue <= 90);

              if(!validLatitude)
                  sendToast("Niepoprawna wartosc dlugosci geograficznej");

              if(!validLongitude)
                  sendToast("Niepoprawna wartosc szerokosci geograficznej");

              if(validLatitude && validLatitude){
                  latitude = latitudeSpinner.getSelectedItem().equals("N") ? newLatitudeValue : newLatitudeValue * -1;
                  longitude = longitudeSpinner.getSelectedItem().equals("E") ? newLongitudeValue : newLongitudeValue * -1;
                  locationPresenter(true);
                  fragmentsUpdate(); // COMMENT TO DISABLE REFRESH
              }

              Log.d(TAG, "" + selectedTimerInterval);
              if(timerInterval != selectedTimerInterval){
                  timerInterval = selectedTimerInterval;
                  runFragmentRefresher(timerInterval);
              }

          }

        });
      }

    @SuppressLint("SetTextI18n")
    private void locationPresenter(boolean update) {
        if (update == true) {
            latitudeInput.setText(String.valueOf(Math.abs(latitude)));
            longitudeInput.setText(String.valueOf(Math.abs(longitude)));
        }
        locElement.setText(Math.abs(latitude) + (latitude > 0 ? "′N" : "′S") + "  " + Math.abs(longitude) + (longitude > 0 ? "′E" : "′W"));
    }


    public void runFragmentRefresher(Integer timerInterval){
        if(fragmentTimer != null)
            fragmentTimer.cancel();
        fragmentTimer = new Timer();
        FragmentsUpdateTimerTask fragmentUpdateTimerTask = new FragmentsUpdateTimerTask();
        fragmentTimer.schedule(fragmentUpdateTimerTask, 0, timerInterval);

    }

    public void initAstro(){
        Calendar rightNow = Calendar.getInstance();
        astroTime = new AstroDateTime(rightNow.get(Calendar.YEAR), rightNow.get(Calendar.MONTH)+1, rightNow.get(Calendar.DAY_OF_MONTH), rightNow.get(Calendar.HOUR), rightNow.get(Calendar.MINUTE), rightNow.get(Calendar.SECOND), 1, true);
        location = new AstroCalculator.Location(latitude, longitude);
        astro = new AstroCalculator(astroTime, location);
    }

    public AstroCalculator getUpdatedAstro(){

        Calendar rightNow = Calendar.getInstance();
        astroTime = new AstroDateTime(rightNow.get(Calendar.YEAR), rightNow.get(Calendar.MONTH)+1, rightNow.get(Calendar.DAY_OF_MONTH), rightNow.get(Calendar.HOUR), rightNow.get(Calendar.MINUTE), rightNow.get(Calendar.SECOND), 1, true);
        location = new AstroCalculator.Location(latitude, longitude);
        astro = new AstroCalculator(astroTime, location);
        return astro;
    }

    private class FragmentsUpdateTimerTask extends TimerTask {
        @Override
        public void run() {
            fragmentsUpdate();
        }

    }

    private void fragmentsUpdate() {
        AstroCalculator astro = getUpdatedAstro();
        Log.d(TAG, "\t Update !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        if(sun != null && moon != null){
            sun.updateAstro(astro.getSunInfo());
            moon.updateAstro(astro.getMoonInfo());
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @TargetApi(Build.VERSION_CODES.O)
        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return sun;
            else
                return moon;
        }

        public CharSequence getPageTitle(int position) {
            if (position == 0)
                return "Słońce";
            else
                return "Księżyc";
        }

        @Override
        public int getCount() {
            return 2;
        }
    }



//    Build Sun Fragment
    public Sun buildSun(){
        Sun sun = new Sun();
        Bundle args = new Bundle();
        args.putString("sunrise", timeFormatter(astro.getSunInfo().getSunrise().getHour(), astro.getSunInfo().getSunrise().getMinute()));
        args.putString("sunset", timeFormatter(astro.getSunInfo().getSunset().getHour(), astro.getSunInfo().getSunset().getMinute()));
        args.putString("morningTwilight", timeFormatter(astro.getSunInfo().getTwilightMorning().getHour(), astro.getSunInfo().getTwilightMorning().getMinute()));
        args.putString("eveningTwilight", timeFormatter(astro.getSunInfo().getTwilightEvening().getHour(), astro.getSunInfo().getTwilightEvening().getMinute()));

        args.putString("sunriseAzimuth", azimuthFormatter(astro.getSunInfo().getAzimuthRise()));
        args.putString("sunsetAzimuth", azimuthFormatter(astro.getSunInfo().getAzimuthSet()));

        sun.setArguments(args);
        return sun;
    }

//      Build Moon Fragment
    public Moon buildMoon(){
        Moon moon= new Moon();
        Bundle args = new Bundle();
        args.putString("moonrise", timeFormatter(astro.getMoonInfo().getMoonrise().getHour(), astro.getMoonInfo().getMoonrise().getMinute()));
        args.putString("moonset", timeFormatter(astro.getMoonInfo().getMoonset().getHour(), astro.getMoonInfo().getMoonset().getMinute()));
        args.putString("nextMoon", dateFormatter(astro.getMoonInfo().getNextNewMoon().getDay(), astro.getMoonInfo().getNextNewMoon().getMonth(), astro.getMoonInfo().getNextNewMoon().getYear()));
        args.putString("fullMoon", dateFormatter(astro.getMoonInfo().getNextFullMoon().getDay(), astro.getMoonInfo().getNextFullMoon().getMonth(), astro.getMoonInfo().getNextFullMoon().getYear()));

        args.putString("illumination", String.format("%.2f ", astro.getMoonInfo().getIllumination() * 100 ) + "%");
        args.putString("moonAge", (int) Math.round(astro.getMoonInfo().getAge()*(-1)) + " dzień");

        moon.setArguments(args);
        return moon;
    }



    private String dateFormatter(int day, int month, int year) {
        return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + year;
    }

    public String timeFormatter(int hour, int minute) {
        return hour + ":" + (minute < 10 ? "0" + minute : minute);
    };

    public String azimuthFormatter(double azimuth) {
        return String.format("%.2f°", azimuth);
    }
}
